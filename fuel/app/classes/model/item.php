<?php
class Model_Item extends Orm\Model
{
	protected static $_primary_key = array('item_number');
	protected static $_table_name = 'item';
    protected static $_properties = array('item_number', 'created_at', 'updated_at', 'cost','discount','purchase_order_id');

    public static function get_items_by_order_id($order_id)
    {
        // Database interactions
        $result = DB::query("SELECT `item_number`,`created_at`,`updated_at`,`cost`,`discount` FROM `item` WHERE `purchase_order_id` = $order_id", DB::SELECT)->execute();

        return $result->as_array();
    }
    public static function get_item_numbers_by_order_id($order_id)
    {
        // Database interactions
        $result = DB::query("SELECT `item_number` FROM `item` WHERE `purchase_order_id` = $order_id", DB::SELECT)->execute();

        return $result->as_array();
    }
    public static function exists_item_for_order($item_number,$order_id)
    {
        // Database interactions
        $result = DB::query("SELECT `item_number` FROM `item` WHERE `purchase_order_id` = $order_id AND `item_number` = $item_number", DB::SELECT)->execute();

        return $result->as_array();
    }
    public static function update_values($cost,$discount,$item_number) {
        $result = DB::update('item')
                ->set(array(
                    'cost'  => $cost,
                    'discount' => $discount,
                    'updated_at' => date('Y-m-d H:i:s')
                ))
                ->where('item_number', '=', $item_number)
                ->execute();
    }
    public static function delete_item($item_number) {
        $result = DB::delete('item')
                ->where('item_number', '=', $item_number)
                ->execute();
    }
    public static function create_item($item_number,$cost,$discount,$order_id) {
        // Database interactions
        list($insert_id) = DB::insert('item')->set(array(
            'item_number' => $item_number,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'cost' => $cost,
            'discount' => $discount,
            'purchase_order_id' => $order_id
        ))->execute();

        return $insert_id;
    }
}
?>