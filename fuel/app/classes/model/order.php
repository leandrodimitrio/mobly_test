<?php
class Model_Order extends \Model
{
	public static function get_all_orders()
    {
        // Database interactions
        $result = DB::query('SELECT `order_number`,`created_at`,`total_cost`,`total_discount` FROM `purchase_order` ORDER BY `order_number` DESC', DB::SELECT)->execute();

        return $result->as_array();
    }
    public static function create_new_order($total_cost = 0,$total_discount = 0)
    {
        // Database interactions
        list($insert_id, $rows_affected) = DB::insert('purchase_order')->set(array(
		    'created_at' => date('Y-m-d H:i:s'),
		    'updated_at' => date('Y-m-d H:i:s'),
		    'total_cost' => $total_cost,
		    'total_discount' => $total_discount
		))->execute();

		return $insert_id;
    }
    public static function update_totals($total_cost,$total_discount,$order_number) {
        $result = DB::update('purchase_order')
                ->set(array(
                    'total_cost'  => $total_cost,
                    'total_discount' => $total_discount,
                    'updated_at' => date('Y-m-d H:i:s')
                ))
                ->where('order_number', '=', $order_number)
                ->execute();
    }
    public static function delete_order($order_number) {
        $result = DB::delete('purchase_order')
                ->where('order_number', '=', $order_number)
                ->execute();
    }
}
?>