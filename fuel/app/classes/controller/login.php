<?php

class Controller_Login extends Controller
{
    public function action_index()
    {
        // First we'll check if this dude is logged-in already
        if ( ! Auth::check())
        {
            // Let's set a couple of View variables
            $data = array(
            'title' => 'Faça seu login',
            'base_url' => Uri::base(false),
            );

            //Now render the views
            echo View::forge('templates/header',$data);
            echo View::forge('login',$data);
            echo View::forge('templates/footer',$data);
        } else { // So they're already logged in
            // And they're trying to logout
            if(Input::get('action', 'logout')) {
                Auth::logout();
                Response::redirect('/login');
            // If not, just go to the Dashboard
            } else {
                Response::redirect('/dashboard');
            }
        }
    }
    public function action_auth() {
    // Now we're checking whether this is a post or not
        if (Input::post())
        {
            // If it's a post, let's try to log them in
            if (Auth::login(Input::post('email'),Input::post('password')))
            {
                Response::redirect('/dashboard');
            }
            else
            {
                $data['email']    = Input::post('email');
                $data['login_error'] = 'Combinação errada de email e senha. Por favor, tente novamente.';
            }
        // If they're not trying to post, take 'em back
        } else {
            Response::redirect('/login');
        }
    }
}