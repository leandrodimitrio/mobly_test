<?php

class Controller_Order extends Controller
{
    public function action_new()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            // We'll set a couple of View variables
            $data = array(
            'title' => 'Nova venda | Mobly',
            'base_url' => Uri::base(false),
            );

            $data['items_amount'] = Input::get('items_amount') ? Input::get('items_amount') : 1;

            $data['js'] = '$(".order_save").submit(function(e){
                var values = [];

                $(".item_number").each(function() {

                    if ( $.inArray(this.value, values) >= 0 ) {

                        alert("Os números de itens devem ser únicos.");
                        e.preventDefault();

                    } else {

                        values.push( this.value );
                    }
                });
            });';

            //Now render the views
            echo View::forge('templates/header',$data);
            echo View::forge('order_new',$data);
            echo View::forge('templates/footer',$data);
        }
    }
    
    public function action_save()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            //Only makes sense to continue if this is POST
            if (Input::post())
            {
                $items_amount = Input::post('items_amount');

                $i=1;
                $items_array = array();
                //We'll go through all the items coming from post, and we'll throw them into an array
                while($i <= Input::post('items_amount')):
                    $item_number = Input::post("item_number|$i");
                    $cost = Input::post("cost|$i");
                    $discount = Input::post("discount|$i");

                    $items_array[$i] = array(
                        'item_number' => $item_number,
                        'cost' => $cost,
                        'discount' => $discount,
                        );

                    $i++;
                endwhile;

                $order_number = Model_Order::create_new_order();

                // Now let's create the items, and use the order number
                $total_cost = $total_discount = 0;
                foreach ($items_array as $ia) {
                    global $total_cost, $total_discount;
                    $new_item = new Model_Item();
                    $new_item->item_number = $ia['item_number'];
                    $new_item->created_at = date('Y-m-d H:i:s');
                    $new_item->updated_at = date('Y-m-d H:i:s');
                    $new_item->cost = floatval(str_replace(",", ".",$ia['cost']));
                    $total_cost = $total_cost + $new_item->cost;

                    $new_item->discount = floatval(str_replace(",", ".",$ia['discount']));
                    $total_discount = $total_discount + $new_item->discount;

                    $new_item->purchase_order_id = $order_number;
                    $new_item->save();
                }

                Model_Order::update_totals($total_cost,$total_discount,$order_number);

                Response::redirect('/dashboard');

                // We'll run a check whether the item number exists
                //  $exists_item_number = FALSE;
                //  foreach ($items_array as $ia) {
                //      foreach ($ia as $key => $value) {
                //          if($key == "item_number") {
                //              $entry = Model_Item::find('all', array(
                //                  'where' => array(
                //                      array('item_number', $value)
                //                  )
                //              ));
                //              if(!empty($entry)) { $exists_item_number = TRUE; }
                //          }
                //      }
                //  }
                // if(!$exists_item_number) {

                // }
            } else {
                Response::redirect('/order/new');
            }
        }
    }
    public function action_edit()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            if (!Input::post()) {
                //Let's check if there's something to be edited
                if(Uri::segment(3)) {
                    $data = array(
                    'title' => 'Edição de Venda: '.Uri::segment(3).' | Mobly',
                    'base_url' => Uri::base(false)
                    );
                    if(Input::get('item_exists')){
                        $data['item_exists'] = Input::get('item_exists');
                    }
                    $data['order_number'] = Uri::segment(3);

                    $data['items_in_order'] = Model_Item::get_items_by_order_id( Uri::segment(3));

                    //Now render the views
                    echo View::forge('templates/header',$data);
                    echo View::forge('order_edit',$data);
                    echo View::forge('templates/footer',$data);
                } else {
                    Response::redirect('/dashboard');
                }
            } else {
                // Okay, so this is a post, which means it's being edited
                $order_item_numbers = Model_Item::get_item_numbers_by_order_id(Uri::segment(3));
                $total_cost = $total_discount = 0;
                foreach ($order_item_numbers as $oin) {
                    $cost = floatval(str_replace(",", ".",Input::post("cost|".$oin['item_number'])));
                    $total_cost = $total_cost + $cost;

                    $discount = floatval(str_replace(",", ".",Input::post("discount|".$oin['item_number'])));
                    $total_discount = $total_discount + $discount;

                    Model_Item::update_values($cost,$discount,$oin['item_number']);
                }
                Model_Order::update_totals($total_cost,$total_discount,Uri::segment(3));
                Response::redirect('/dashboard');
            }
        }
    }
    public function action_delete()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            if(Uri::segment(3)) {
                $order_item_numbers = Model_Item::get_item_numbers_by_order_id(Uri::segment(3));

                foreach ($order_item_numbers as $oin) {
                    Model_Item::delete_item($oin['item_number']);
                }

                Model_Order::delete_order(Uri::segment(3));
                Response::redirect('/dashboard');
            }
        }
    }
}