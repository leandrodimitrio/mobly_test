<?php

class Controller_Item extends Controller
{
    public function action_new()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            if (Input::post()) {
                if(!Model_Item::exists_item_for_order(Input::post("item_number"),Uri::segment(3)))
                {
                    Model_Item::create_item(Input::post("item_number"),Input::post("cost"),Input::post("discount"),Uri::segment(3));

                    $all_order_items = Model_Item::get_items_by_order_id(Uri::segment(3));

                    $total_cost = $total_discount = 0;
                    foreach ($all_order_items as $aoi) {
                        $total_cost = $total_cost + floatval($aoi['cost']);
                        $total_discount = $total_discount + floatval($aoi['cost']);
                    }

                    Response::redirect('/order/edit/'.Uri::segment(3));
                } else {
                    Response::redirect('/order/edit/'.Uri::segment(3).'?item_exists='.Input::post("item_number"));
                }
                
            } else {
                // We'll set a couple of View variables
                $data = array(
                'title' => 'Novo item | Mobly',
                'base_url' => Uri::base(false),
                'order_id' => Uri::segment(3)
                );

                //Now render the views
                echo View::forge('templates/header',$data);
                echo View::forge('item_new',$data);
                echo View::forge('templates/footer',$data);
            }
        }
    }
    public function action_delete()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            if(Uri::segment(3)) {
                Model_Item::delete_item(Uri::segment(4));

                $all_order_items = Model_Item::get_items_by_order_id(Uri::segment(3));

                $total_cost = $total_discount = 0;
                foreach ($all_order_items as $aoi) {
                    $total_cost = $total_cost + floatval($aoi['cost']);
                    $total_discount = $total_discount + floatval($aoi['cost']);
                }

                Model_Order::update_totals($total_cost,$total_discount,Uri::segment(3));

                Response::redirect('/order/edit/'.Uri::segment(3));
            }
        }
    }
}