<?php

class Controller_Dashboard extends Controller
{
    public function action_index()
    {
        // First we'll check if this dude is logged in already
        if ( ! Auth::check())
        {
            // If not, go back to login
            Response::redirect('/login');
        // Else, let's carry on
        } else {
            // We'll set a couple of View variables
            $data = array(
            'title' => 'Bem-vindo ao Teste da Mobly',
            'base_url' => Uri::base(false),
            );

            $data['js'] = '
            $(document).ready(function(){
                $(".filterable .btn-filter").click(function(){
                    var $panel = $(this).parents(".filterable"),
                    $filters = $panel.find(".filters input"),
                    $tbody = $panel.find(".table tbody");
                    if ($filters.prop("disabled") == true) {
                        $filters.prop("disabled", false);
                        $filters.first().focus();
                    } else {
                        $filters.val("").prop("disabled", true);
                        $tbody.find(".no-result").remove();
                        $tbody.find("tr").show();
                    }
                });

                $(".filterable .filters input").keyup(function(e){
                    /* Ignore tab key */
                    var code = e.keyCode || e.which;
                    if (code == "9") return;
                    /* Useful DOM data and selectors */
                    var $input = $(this),
                    inputContent = $input.val().toLowerCase(),
                    $panel = $input.parents(".filterable"),
                    column = $panel.find(".filters th").index($input.parents("th")),
                    $table = $panel.find(".table"),
                    $rows = $table.find("tbody tr");
                    /* Dirtiest filter function ever ;) */
                    var $filteredRows = $rows.filter(function(){
                        var value = $(this).find("td").eq(column).text().toLowerCase();
                        return value.indexOf(inputContent) === -1;
                    });
                    /* Clean previous no-result if exist */
                    $table.find("tbody .no-result").remove();
                    /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
                    $rows.show();
                    $filteredRows.hide();
                    /* Prepend no-result row if all rows are filtered */
                    if ($filteredRows.length === $rows.length) {
                        $table.find("tbody").prepend($("<tr class=\'no-result text-center\'><td colspan=\'"+ $table.find(".filters th").length +"\'>No result found</td></tr>"));
                    }
                });
            });';

            // find all articles from category 1 order descending by date

            $data['all_purchases_array'] = Model_Order::get_all_orders();

            //Now render the views
            echo View::forge('templates/header',$data);
            echo View::forge('dashboard',$data);
            echo View::forge('templates/footer',$data);
        }
    }
}