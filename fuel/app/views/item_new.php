    <div class="row">
        <div class="col-md-6">
            <h3>Cadastre um novo item</h3>
            <hr>
            <form role="form" class="order_save" action="<?php echo $base_url;?>item/new/<?php echo $order_id;?>" method="post">
              <div class="form-group">
                <label for="item_number">Número do item</label>
                <input type="text" max="999" class="form-control item_number" name="item_number" placeholder="Número do item" required>
              </div>
              <div class="form-group">
                <label for="cost">Preço</label>
                <input type="text" class="form-control cost" name="cost" id="cost" placeholder="Preço">
              </div>
              <div class="form-group">
                <label for="discount">Desconto</label>
                <input type="text" class="form-control discount" name="discount" id="discount" placeholder="Desconto">
              </div>
              <hr>

              <button type="submit" class="btn btn-default" id="btn_enviar">Salvar</button>
            </form>
        </div>
    </div>
</div>