    <div class="row">
        <div class="col-md-6">
            <h3>Cadastre uma nova venda</h3>
            <form role="form" action="<?php echo $base_url;?>order/new" method="get">
                <div class="form-group">
                    <label for="items_amount">Quantidade de itens</label>
                    <input type="text" class="form-control" name="items_amount" id="items_amount" value="<?php echo $items_amount;?>" placeholder="Quantidade de itens">
              </div>
              <button type="submit" class="btn btn-default">Atualizar</button>
            </form>
            <hr>
            <form role="form" class="order_save" action="<?php echo $base_url;?>order/save" method="post">
                <input type="hidden" name="items_amount" value="<?php echo $items_amount;?>">
                <?php $i = 1;
                 while($i <= $items_amount): ?>
              <div class="form-group">
                <label for="item_number">Número do item</label>
                <input type="text" max="999" class="form-control item_number" name="item_number|<?php echo $i;?>" placeholder="Número do item" required>
              </div>
              <div class="form-group">
                <label for="cost">Preço</label>
                <input type="text" class="form-control cost" name="cost|<?php echo $i;?>" id="cost" placeholder="Preço">
              </div>
              <div class="form-group">
                <label for="discount">Desconto</label>
                <input type="text" class="form-control discount" name="discount|<?php echo $i;?>" id="discount" placeholder="Desconto">
              </div>
              <hr>
          <?php $i++; endwhile; ?>

              <button type="submit" class="btn btn-default" id="btn_enviar">Salvar</button>
            </form>
        </div>
    </div>
</div>