    <div class="row">
        <div class="col-md-6">
          <?php if(isset($item_exists)) {?><div class="alert alert-danger">Item <?php echo $item_exists;?> já existe</div><?php } ?>
            <h3>Edição de venda - <?php echo $order_number;?></h3>
            <h5><a href="<?php echo $base_url;?>item/new/<?php echo $order_number;?>">Criar novo item</a></h5>
            <hr>
            <form role="form" class="order_save" action="<?php echo $base_url;?>order/edit/<?php echo $order_number;?>" method="post">
                <?php foreach ($items_in_order as $iio) {?>
              <div class="form-group">
                <a class="pull-right" href="<?php echo $base_url;?>item/delete/<?php echo $order_number;?>/<?php echo $iio['item_number'];?>" onclick="return confirm('Tem certeza que deseja deletar este item?')"><i class="fa fa-trash"></i> Remover item</a></td>
                <label for="item_number">Número do item</label>
                <input type="text" max="999" class="form-control item_number" name="item_number|<?php echo $iio['item_number'];?>" value="<?php echo $iio['item_number'];?>" disabled>
              </div>
              <div class="form-group">
                <label for="cost">Preço</label>
                <input type="text" class="form-control cost" name="cost|<?php echo $iio['item_number'];?>" id="cost" placeholder="Preço" value="<?php echo $iio['cost'];?>">
              </div>
              <div class="form-group">
                <label for="discount">Desconto</label>
                <input type="text" class="form-control discount" name="discount|<?php echo $iio['item_number'];?>" id="discount" value="<?php echo $iio['discount'];?>" placeholder="Desconto">
              </div>
              <hr>
          <?php } ?>

              <button type="submit" class="btn btn-default" id="btn_enviar">Salvar</button>
            </form>
        </div>
    </div>
</div>