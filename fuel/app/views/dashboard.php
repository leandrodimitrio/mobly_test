    <h3>Todas as compras</h3>
    
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Compras</h3>
                <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr class="filters">
                        <th><input type="text" class="form-control" placeholder="Núm. da Compra" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Data da Compra" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Valor Total" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Desconto Total" disabled></th>
                        <th>Editar</th>
                        <th>Apagar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(empty($all_purchases_array)) {?>
                        <tr colspan="4">
                            <td>Nenhuma compra no momento</td>
                        </tr>
                    <?php } else { ?>
                            <?php 
                            $i = 0;
                            while($i < count($all_purchases_array)) {?>
                            <tr>
                                <?php foreach ($all_purchases_array[$i] as $key => $value) {
                                    // We need to store the order_number to a variable, as we loop
                                    if($key == "order_number") { $order_number = $value; } ?>
                                    <td class="<?php echo $key;?>"><?php echo $value;?></td>
                                <?php } ?>
                                <td><a href="<?php echo $base_url;?>order/edit/<?php echo $order_number;?>"><i class="fa fa-pencil-square-o"></i></a></td>
                                <td><a href="<?php echo $base_url;?>order/delete/<?php echo $order_number;?>" onclick="return confirm('Tem certeza que deseja deletar a compra e seus itens?')"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        <?php $i++; } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>