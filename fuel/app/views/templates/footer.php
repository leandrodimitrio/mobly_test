	<script src="<?php echo $base_url;?>assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo $base_url;?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo $base_url;?>assets/js/jquery.mask.min.js"></script>

    <script>
    $('#item_number').mask('000');
    $('.total_cost,.total_discount,.cost,.discount').mask('000.000.000.000.000,00', {reverse: true});
    </script>
    
    <!-- START CUSTOM SCRIPTS -->
    <?php
    // We're checking to see if there's any extra JS being sent from the controller. If so, then echo it
    if(isset($js)) {?>
    <script>
    <?php echo htmlspecialchars_decode($js); ?>
    </script>
	<?php } ?>

	<!-- END CUSTOM SCRIPTS -->

  </body>
</html>